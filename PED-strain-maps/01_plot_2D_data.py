"""
This script reads the data used in Fig. 7 and produces a basic plot.

Stefan Sandfeld (2019), The Micromechanical Materials Modelling group, TU Bergakademie Freiberg.
"""
import numpy as np
import matplotlib.pyplot as plt


def fine_tune_color_bar(ax, im):
    """Resize and re-position the color bar, adjust font size of numbers
    
    :param ax: matplotlib axis instance
    :param im: `imshow` object for which to show the colorbar
    """
    cb = plt.colorbar(im, cax=ax, fraction=0.95)
    pos1 = cb.ax.get_position()
    pos2 = [pos1.x0+0.02, pos1.y0+0.08, pos1.width, pos1.height-0.3]
    cb.ax.set_position(pos2)
    for t in cb.ax.get_yticklabels():
        t.set_fontsize(11)


def main():
    # Read the TEM image and the digitized PED strain maps from the files
    img = plt.imread('fig7a_tem_image.png')
    strain_xx = np.loadtxt('strain_xx.txt')
    strain_yy = np.loadtxt('strain_yy.txt')
    strain_xy = np.loadtxt('strain_xy.txt')

    # create figure and set up subplots
    gridspec_kw={'right': 0.8, 'wspace': 0.05, 'hspace': 0.03, 'width_ratios': [1, 1, 0.06]}
    fig, ax = plt.subplots(ncols=3, nrows=2, figsize=(6.5, 5), gridspec_kw=gridspec_kw)
    ax[0, 2].remove()
    ax[1, 2].remove()
    gs = ax[0, 0].get_gridspec()
    cax = fig.add_subplot(gs[:, -1])

    # show grayscale TEM image
    ax[0, 0].imshow(img, extent=(0, 74, 67, 0))

    # show strain fields
    im_kws = {'cmap': 'RdBu_r', 'vmin': -0.007, 'vmax': 0.007}
    im1 = ax[0, 1].imshow(strain_xx, **im_kws)
    im2 = ax[1, 0].imshow(strain_yy, **im_kws)
    im3 = ax[1, 1].imshow(strain_xy, **im_kws)

    # add colorbar, adjust position and font size
    fine_tune_color_bar(cax, im1)

    # remove ticks and ticklabels and add strain labels
    for axis, label in zip(ax[:2, :2].flatten(), [r'{}', r'{xx}', r'{yy}', r'{xy}']):
        axis.set(xticks=[], yticks=[], xticklabels=[], yticklabels=[])
        axis.text(60, 63, r"$\varepsilon_{}$".format(label), color='0', fontsize=20)   

    plt.show()


if __name__ == "__main__":
    main()