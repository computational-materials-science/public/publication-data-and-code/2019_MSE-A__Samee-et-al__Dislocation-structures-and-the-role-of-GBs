"""
This script reads the data used in Fig. 7, averages along the y-direction 
and plots the normal strain epsilon_yy.

Stefan Sandfeld (2019), The Micromechanical Materials Modelling group, TU Bergakademie Freiberg.
"""
import numpy as np
import matplotlib.pyplot as plt
from itertools import product


# settings for the whole figure
gridspec_kw={'left': 0.08, 'bottom': 0.15, 'wspace': 0.5, 'width_ratios': [1, 1.3]}

# setting for imshow plots
im_kws = {'cmap': 'RdBu_r', 'vmin': -0.007, 'vmax': 0.007}

# define the range of pixel rows that are used for averaging the strain in the wall
rows = range(20, 50) 


def main():
    # Read the TEM image and the digitized PED strain maps from the files
    plt.imread('fig7a_tem_image.png')
    strain_xx = np.loadtxt('strain_xx.txt')

    # create figure and set up subplots
    fig, (ax0, ax1) = plt.subplots(ncols=2, figsize=(9, 3), gridspec_kw=gridspec_kw)

    # left figure: plot originial xx strain
    ax0.imshow(strain_xx, **im_kws)

    # left figure: indicate the region limited 
    z = np.zeros_like(strain_xx)
    z[rows, :] = strain_xx[rows, :]
    ax0.imshow(z, alpha=0.8, **im_kws)
    ax0.set(xlabel='x [pixel]', ylabel='y [pixel]', title=r'strain $\varepsilon_{\rm xx}$')

    # right figure: strain averaged along vertical axis
    av_strain_xx = np.mean(strain_xx[rows,:], axis=0)
    ax1.plot(av_strain_xx, c='C3', lw=2)
    ax1.plot(strain_xx[rows,:].T, c='0', alpha=0.05)
    ax1.set(xlabel='x [pixel]', ylabel=r'average strain $\langle\varepsilon_{\rm xx}\rangle$', 
            xlim=(0, len(av_strain_xx) - 1), ylim=(-0.003, 0.005),
            title=r'average strain $\langle\varepsilon_{xx}\rangle_{y}$')
    ax1.axhline(0, alpha=0.2, lw=1, ls='-', c='0')

    # show the width of the wall structure
    for ax, x0 in product((ax0, ax1), (30, 40)):
        ax.axvline(x0, lw=2, ls='--', alpha=0.5)

    plt.show()


if __name__ == "__main__":
    main()