# 2019-MSE-A__Dislocation-structures-and-the-role-of-GBs

## Prerequisites
The plot and analysis scripts and "notebooks" are written in Python and should 
work with any python version >= 3.5.

The `requirements.txt` file contains all Python libraries that the scripts and 
notebooks depend on. The requirements can be installed using:

```
pip install -r requirements.txt
```

## How To:
The directory `PED-strain-maps` contains all data used for plotting Fig. 7 in 
the manuscript. 

- The two python scripts (`01_...py` and `02_...py`) show how to 
read, plot, and analyze the data. 
- The Jupyter Notebook `03_...ipynb` require 
that the jupyter packages are installed and contain additional information. 
Usually GitLab should also be able to show the content for the jupyter notebook
properly.

The directory `Strain-analysis` contains supplementary plots for Fig. 8 and 
shows the influence of randomness and the total number of dislocations that are
contained in the "wall" strucutre.


The directory `Dislocation-at-GB` contains data and the Jupyter notebook for 
reproducing the plots shown in Fig. 10 in the manuscript.



## Data
The TEM data is published under the Creative Commons Attribution (CC BY) on 
xx.yy.2019 in https://www.elsevier.com/journals/data-in-brief. For simulation 
data the same license as also for the software applies.

## Acknowledgements
Financial support from the Flemish (FWO) and German Research Foundation (DFG) 
through the European M-ERA.NET project “FaSS” (Fatigue Simulation near Surfaces) 
under the grant numbers GA.014.13 N, SCHW855/5-1, and SA2292/2-1 is gratefully 
acknowledged. Furthermore, Stefan Sandfeld acknowledges financial support from 
the European Research Council through the ERC Grant Agreement No. 759419 
(MuDiLingo – A Multiscale Dislocation Language for Data- Driven Materials 
Science).